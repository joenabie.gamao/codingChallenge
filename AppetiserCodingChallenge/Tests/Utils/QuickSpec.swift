import Quick
import AppetiserCodingChallenge

extension QuickSpec {
  
  func jsonDictionaryFromFile(_ name: String) -> JSONDictionary {
    return AppetiserCodingChallenge.jsonDictionaryFromFile(name, bundle: Bundle(for: type(of: self)))
  }
  
}
