//
//  Album.swift
//  AppetiserCodingChallenge
//
//  Created by Joenabie Gamao on 12/11/2019.
//  Copyright © 2019 Appetiser Pty Ltd. All rights reserved.
//
import Foundation

class Album: APIModel, Codable {
    var artworkUrl100: String
    var artworkUrl60: String
    var trackPrice: Double
    var trackName: String
    var longDescription: String
    var primaryGenreName: String
  
  enum CodingKeys: String, CodingKey {
      case artworkUrl100
      case artworkUrl60
      case primaryGenreName
      case trackName
      case trackPrice
      case longDescription
  }
  
  init(trackName: String, artwork100: String, price: Double, genre: String, longDescription: String) {
      self.primaryGenreName = genre
      self.artworkUrl100 = artwork100
      self.artworkUrl60 = artwork100
      self.trackName = trackName
      self.trackPrice = price
      self.longDescription = longDescription
  }
  
   required init(from decoder: Decoder) throws {
      let container = try decoder.container(keyedBy: CodingKeys.self)
      trackName = try container.decodeIfPresent(String.self, forKey: .trackName) ?? ""
      artworkUrl100 = try container.decodeIfPresent(String.self, forKey: .artworkUrl100) ?? ""
      artworkUrl60 = try container.decodeIfPresent(String.self, forKey: .artworkUrl60) ?? ""
      primaryGenreName = try container.decodeIfPresent(String.self, forKey: .primaryGenreName) ?? ""
      longDescription = try container.decodeIfPresent(String.self, forKey: .longDescription) ?? ""
      trackPrice = try container.decodeIfPresent(Double.self, forKey: .trackPrice) ?? 0
  }
  
  struct ApiResults: Decodable {
      let results: [Album]
  }
  
  /// Wrapper for decoding data in array
  public static func decodeArray(data: Data) -> [Album]? {

      return decode(ApiResults.self, from: data)?.results ?? decode([Album].self, from: data)
  }
}
