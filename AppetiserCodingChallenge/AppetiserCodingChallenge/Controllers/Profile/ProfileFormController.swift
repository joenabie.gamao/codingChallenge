//
//  ProfileFormController.swift
//  AppetiserCodingChallenge
//
//  Created by Joenabie Gamao ( https://maxineer.com )
//  Copyright © 2019 "Appetiser Pty Ltd". All rights reserved.
//

import UIKit

class ProfileFormController: UITableViewController, ImagePickerPresenter {
  
  var imagePicker: UIImagePickerController
  
  private var image: UIImage?
  
  required init?(coder aDecoder: NSCoder) {
    imagePicker = UIImagePickerController()
    super.init(coder: aDecoder)
    
    imagePicker.delegate = self
  }
  
  @IBAction
  func editPhotoButtonTapped(_ sender: AnyObject) {
    // Call this if you want to give the user a chance to select the source of the photo.
    presentPickerOptionsSheet()
    // or just from the camera...
    //presentPhotoCaptureScreen()
    // or from the photo library.
    //presentPhotoLibrary()
  }
  
}

extension ProfileFormController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
  
  func imagePickerController(
    _ picker: UIImagePickerController,
    didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]
  ) {
    if let editedImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
      self.image = editedImage
    }
  }
  
  func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
    dismiss(animated: true)
  }
  
}
