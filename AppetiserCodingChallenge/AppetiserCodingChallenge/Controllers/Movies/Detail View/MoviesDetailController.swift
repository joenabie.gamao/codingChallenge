//
//  MoviesDetailController.swift
//  AppetiserCodingChallenge
//
//  Created by Joenabie Gamao on 13/11/2019.
//  Copyright © 2019 Appetiser Pty Ltd. All rights reserved.
//

import UIKit

import PureLayout
import AlamofireImage

/// Shows the full details of a Movie
class MoviesDetailController: UIViewController {
  var viewModel: AlbumViewModel!
  
  @IBOutlet weak var trackNameLabel: UILabel!
  @IBOutlet weak var imageView: UIImageView!
  @IBOutlet weak var priceLabel: UILabel!
  @IBOutlet weak var genreLabel: UILabel!
  @IBOutlet weak var descriptionLabel: UILabel!

  override func viewDidLoad() {
    super.viewDidLoad()
    setup()
  }
  
}

// MARK: Setup
extension MoviesDetailController {
  private func setup() {
    setupNavAndBG()
    setupVM()
  }
  
  private func setupVM() {
    trackNameLabel.text = viewModel.trackName
    if let url = viewModel.hqImageURL {
      imageView.af_setImage(withURL: url)
    }
    priceLabel.text = viewModel.priceStr
    genreLabel.text = viewModel.genre
    descriptionLabel.text = viewModel.longDescription
  }

  private func setupNavAndBG() {
    view.backgroundColor = .white
    navigationItem.largeTitleDisplayMode = .never
  }
}
