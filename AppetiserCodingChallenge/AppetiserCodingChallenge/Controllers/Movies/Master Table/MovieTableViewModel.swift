//
//  MovieTableViewModel.swift
//  AppetiserCodingChallenge
//
//  Created by Joenabie Gamao on 18/11/2019.
//  Copyright © 2019 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

protocol MovieTableViewModelType {
  var delegate: MovieTableViewModelTypeDelegate? { get set }
  var lastSavedAlbumViewModel: AlbumViewModel? { get set }
  func refreshData()
}

protocol MovieTableViewModelTypeDelegate: AnyObject {
  func viewModel(_ viewModel: MovieTableViewModelType, didFetchData data: [Album])
  func viewModel(_ viewModel: MovieTableViewModelType, didEncounter error: Error)
}


class MovieTableViewModel: MovieTableViewModelType {
  weak var delegate: MovieTableViewModelTypeDelegate?
  var lastSavedAlbumViewModel: AlbumViewModel?
  private let api: APIClient
  
  init(api: APIClient = App.shared.api) {
    self.api = api
  }
  
  func refreshData() {
    api.searchItunes(query: URL.defaultTerm) { [weak self] albums, error in
      guard let weakSelf = self else { return }
      if let err = error, let delegate = weakSelf.delegate {
        print(err)
        return delegate.viewModel(weakSelf, didEncounter: err)
      }
      
      guard let albums = albums else { return }
      weakSelf.delegate?.viewModel(weakSelf, didFetchData: albums)
    }
  }
  
}
