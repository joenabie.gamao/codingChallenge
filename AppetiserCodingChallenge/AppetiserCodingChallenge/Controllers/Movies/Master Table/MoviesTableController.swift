//
//  MoviesTableController.swift
//  AppetiserCodingChallenge
//
//  Created by Joenabie Gamao on 12/11/2019.
//  Copyright © 2019 Appetiser Pty Ltd. All rights reserved.
//

import UIKit
import PureLayout
import Alamofire

class MoviesTableController: UITableViewController {
  let dataSource = MoviesDataSource()
  var viewModel: MovieTableViewModelType!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    setup()
  }
  
  // Load last visited album
  func loadLastVisitedAlbum() {
    let ud = UserDefaults.standard
    if
      let data = ud.value(forKey: Constants.UserDefaults.lastSavedAlbum) as? Data,
      let album = try? JSONDecoder().decode(Album.self, from: data) {
      
      let date = ud.object(forKey: Constants.UserDefaults.lastSavedDate) as? Date ?? Date()
      let viewModel = AlbumViewModel(album: album, lastVisitedDate: date, fromUserDefaults: true)
      let vc = R.storyboard.movies.moviewDetailController()!
      vc.viewModel = viewModel
      navigationController?.pushViewController(vc, animated: true)
    }
  }
  
}

// MARK: Setup
extension MoviesTableController {
  private func setup() {
    setupNavbar()
    setupTableView()
    setupVM()
    loadLastVisitedAlbum()
  }
  
  private func setupVM() {
    viewModel.delegate = self
    viewModel.refreshData()
  }
  
  private func setupNavbar() {
    navigationController?.navigationBar.prefersLargeTitles = true
    navigationItem.title = S.moviesTableTitle()
  }
  
  private func setupTableView() {
    tableView.dataSource = dataSource
    tableView.delegate = self
    // Register our reusable UITableViewCells here.
    tableView.register(MovieCell.self, forCellReuseIdentifier: MovieCell.reuseIdentifier)
  }
}

// MARK: TableView Delegate
extension MoviesTableController {
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    let object = dataSource.albums[indexPath.row]
    let vm = AlbumViewModel(album: object)
    vm.saveToUserDefaults()
    let moviesDetailVC = R.storyboard.movies.moviewDetailController()!
    moviesDetailVC.viewModel = vm
    navigationController?.pushViewController(moviesDetailVC, animated: true)
  }
  
  override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 120
  }
}

extension MoviesTableController: MovieTableViewModelTypeDelegate {
  func viewModel(_ viewModel: MovieTableViewModelType, didFetchData data: [Album]) {
    dataSource.data.value = data
    DispatchQueue.main.async { [weak self] in self?.tableView.reloadData() }
  }
  func viewModel(_ viewModel: MovieTableViewModelType, didEncounter error: Error) {
    // Error Handling
    print(error)
  }
}
