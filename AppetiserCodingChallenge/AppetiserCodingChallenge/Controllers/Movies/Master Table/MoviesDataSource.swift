//
//  MoviesDataSource.swift
//  AppetiserCodingChallenge
//
//  Created by Joenabie Gamao on 13/11/2019.
//  Copyright © 2019 Appetiser Pty Ltd. All rights reserved.
//
import UIKit

class MoviesDataSource: GenericDataSource<Album>, UITableViewDataSource {

  var albums: [Album] {
    return data.value
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return albums.count
  }

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    guard let cell = tableView.dequeueReusableCell(
      withIdentifier: MovieCell.reuseIdentifier, for: indexPath) as? MovieCell else {
      return UITableViewCell()
    }
    let album = albums[indexPath.row]
    let vm = AlbumViewModel(album: album)
    cell.viewModel = vm
    return cell
  }
  
}
