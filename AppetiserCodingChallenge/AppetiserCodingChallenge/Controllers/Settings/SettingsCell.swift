//
//  SettingsCell.swift
//  AppetiserCodingChallenge
//
//  Created by Joenabie Gamao ( https://maxineer.com )
//  Copyright © 2019 "Appetiser Pty Ltd". All rights reserved.
//

import UIKit

enum SettingsItemIdentifier {
  case editProfile
  case termsAndConditions
  case privacyPolicy
  case logOut
}

class SettingsCellViewModel {
  
  enum `Type` {
    
    /// Title only. No accessory or whatsoever.
    case titleOnly
    
    /// A cell w/ a title and a helper text below it.
    case titleWithSubtitle
    
    /// A cell w/ a title and a disclosure indicator.
    case disclosing
    
    case custom
  }
  
  let identifier: SettingsItemIdentifier
  let type: Type
  var title: String
  
  var value: String?
  var subtitle: String?
  
  var isEnabled = true
  
  init(
    _ identifier: SettingsItemIdentifier,
    type: Type = .titleOnly,
    title: String = "{title_here}"
  ) {
    self.identifier = identifier
    self.type = type
    self.title = title
  }
  
}

class SettingsCell: UITableViewCell {
  
  var viewModel: SettingsCellViewModel? {
    didSet {
      update()
    }
  }
  
  override func awakeFromNib() {
    super.awakeFromNib()
    reset()
  }
  
  override func prepareForReuse() {
    super.prepareForReuse()
    reset()
  }
  
  func reset() {
    textLabel?.text = nil
    detailTextLabel?.text = nil
    accessoryType = .none
  }
  
  func update() {
    textLabel?.text = viewModel?.title
    
    if viewModel?.type == .titleWithSubtitle {
      detailTextLabel?.text = viewModel?.subtitle
    } else if viewModel?.type == .disclosing {
      accessoryType = .disclosureIndicator
    }
  }
  
}
