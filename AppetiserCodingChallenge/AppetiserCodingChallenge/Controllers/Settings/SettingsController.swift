//
//  SettingsController.swift
//  AppetiserCodingChallenge
//
//  Created by Joenabie Gamao ( https://maxineer.com )
//  Copyright © 2019 "Appetiser Pty Ltd". All rights reserved.
//

import UIKit
import PureLayout
import SVProgressHUD

/// Here's an example of a typical Settings screen with sections. This also demonstrates
/// the use of ControllerViewModel and CellViewModel patterns. No RX.
class SettingsController: TableViewController {
  
  private var viewModel: SettingsViewModel!
  private let settingsCellReuseId = "ruid:settings-cell"
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    title = "Settings"
    
    viewModel = SettingsViewModel()
    viewModel.loadSections()
    
    tableView.rowHeight = 48
    
    // Register our reusable UITableViewCells here.
    tableView.register(SettingsCell.self, forCellReuseIdentifier: settingsCellReuseId)
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    navigationController?.setNavigationBarHidden(false, animated: animated)
  }
  
  // MARK: UITableViewDataSource
  
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return viewModel.sections[section].items.count
  }
  
  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: settingsCellReuseId, for: indexPath)
    
    if let cell = cell as? SettingsCell {
      
      // Let the cell do whatever it wants w/ the model.
      cell.viewModel = viewModel.cellViewModel(for: indexPath)
    }
    
    return cell
  }
  
  override func numberOfSections(in tableView: UITableView) -> Int {
    return viewModel.sections.count
  }
  
  // MARK: UITableViewDelegate
  
  func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
    let model = viewModel.cellViewModel(for: indexPath)
    
    // Title-only cells only display information. So ideally, we shouldn't react to user's tap.
    if model.type == .titleOnly {
      return nil
    }
    
    return indexPath
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    let model = viewModel.cellViewModel(for: indexPath)
    
    if model.identifier == .termsAndConditions {
      // TODO Segue to TOC
    } else if model.identifier == .privacyPolicy {
      // TODO Segue to PP
    } else if model.identifier == .logOut {
      tableView.deselectRow(at: indexPath, animated: true)
      showPreSignOutAlert()
    }
  }
  
}

extension SettingsController {
  
  @objc
  func showPreSignOutAlert() {
    let alert = UIAlertController.init(
      title: S.settingsAlertsLogoutTitle(),
      message: nil,
      preferredStyle: .alert
    )
    
    alert.addAction(UIAlertAction(title: S.cancel(), style: .cancel, handler: nil))
    
    alert.addAction(UIAlertAction(title: S.settingsAlertsButtonsLogout(), style: .default, handler: { (_) in
      SVProgressHUD.show()
      SVProgressHUD.setDefaultMaskType(.clear)
      App.shared.user.logout({ (error) in
        SVProgressHUD.setDefaultMaskType(.none)
        if let error = error {
          SVProgressHUD.showDismissableError(with: error.localizedDescription)
        } else {
          SVProgressHUD.dismiss()
        }
      })
    }))
    
    present(alert, animated: true, completion: nil)
  }

}
