//
//  TraditionalRegistrationController.swift
//  AppetiserCodingChallenge
//
//  Created by Antonio Jr Atamosa on 28/02/2019.
//  Copyright © 2019 "Appetiser Pty Ltd". All rights reserved.
//

import UIKit
import Material

struct PolicyType {
  static let termsAndCondition = "termsAndCondition"
  static let privacyPolicy = "privacyPolicy"
}

class TraditionalRegistrationController: UIViewController {

  struct Segue {
    static let toPrivacy = "toPrivacy"
  }
  
  var policyType: String! = PolicyType.termsAndCondition
  
  @IBOutlet var firstNameTF: ErrorTextField!
  @IBOutlet var lastNameTF: ErrorTextField!
  @IBOutlet var emailTF: ErrorTextField!
  @IBOutlet var passwordTF: TextField!
  @IBOutlet var confirmPasswordTF: TextField!
  @IBOutlet var privacyLabel: UILabel!
  @IBOutlet var submitBtn: Button!
  
  override func viewDidLoad() {
    super.viewDidLoad()

    // Do any additional setup after loading the view.
    initUI()
  }
  
   override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if segue.identifier == Segue.toPrivacy {
      let navController = segue.destination as? UINavigationController
      let controller = navController?.viewControllers.first as? PrivacyController
      controller?.policyType = policyType
    }
   }
 
  func initUI() {
    firstNameTF.placeholder = "First Name"
    firstNameTF.detail = ""
    firstNameTF.isClearIconButtonEnabled = true
    firstNameTF.delegate = self
    firstNameTF.isPlaceholderUppercasedWhenEditing = true
    firstNameTF.placeholderAnimation = .hidden
    
    lastNameTF.placeholder = "Last Name"
    lastNameTF.detail = ""
    lastNameTF.isClearIconButtonEnabled = true
    lastNameTF.delegate = self
    lastNameTF.isPlaceholderUppercasedWhenEditing = true
    lastNameTF.placeholderAnimation = .hidden
    
    emailTF.placeholder = "Email"
    emailTF.detail = ""
    emailTF.isClearIconButtonEnabled = true
    emailTF.delegate = self
    emailTF.isPlaceholderUppercasedWhenEditing = true
    emailTF.placeholderAnimation = .hidden
    
    passwordTF.placeholder = "Password"
    passwordTF.detail = ""
    passwordTF.clearButtonMode = .whileEditing
    passwordTF.isVisibilityIconButtonEnabled = true
    passwordTF.visibilityIconButton?.tintColor = Color.green.base.withAlphaComponent(passwordTF.isSecureTextEntry ? 0.38 : 0.54)
    
    confirmPasswordTF.placeholder = "Confirm Password"
    confirmPasswordTF.detail = ""
    confirmPasswordTF.clearButtonMode = .whileEditing
    confirmPasswordTF.isVisibilityIconButtonEnabled = true
    confirmPasswordTF.visibilityIconButton?.tintColor = Color.green.base.withAlphaComponent(passwordTF.isSecureTextEntry ? 0.38 : 0.54)
    
    let stringValue = (privacyLabel.text)!
    let attributedString = NSMutableAttributedString(attributedString: privacyLabel.attributedText!)
    let termsLinkRange = (stringValue as NSString).range(of: "Terms and Condition")
    let privacyLinkRange = (stringValue as NSString).range(of: "Privacy Policy")
    
    let blueColor = UIColor(red: 0.05, green: 0.4, blue: 0.65, alpha: 1.0)
    attributedString.addAttribute(.foregroundColor, value: blueColor, range: termsLinkRange)
    attributedString.addAttribute(.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: termsLinkRange)
    attributedString.addAttribute(.foregroundColor, value: blueColor, range: privacyLinkRange)
    attributedString.addAttribute(.underlineStyle, value: NSUnderlineStyle.single.rawValue, range: privacyLinkRange)
    privacyLabel.attributedText = attributedString
    
    let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapLabel(_:)))
    tapGesture.numberOfTapsRequired = 1
    tapGesture.numberOfTouchesRequired = 1
    privacyLabel.addGestureRecognizer(tapGesture)
  }

  @objc
  func tapLabel(_ sender: UITapGestureRecognizer) {
    let text = (privacyLabel.text)!
    let termsRange = (text as NSString).range(of: "Terms and Condition")
    let privacyRange = (text as NSString).range(of: "Privacy Policy")
    
    if sender.didTapAttributedTextInLabel(label: privacyLabel, inRange: termsRange) {
      policyType = PolicyType.termsAndCondition
      self.performSegue(withIdentifier: Segue.toPrivacy, sender: nil)
    } else if sender.didTapAttributedTextInLabel(label: privacyLabel, inRange: privacyRange) {
      policyType = PolicyType.privacyPolicy
      self.performSegue(withIdentifier: Segue.toPrivacy, sender: nil)
    } else {
      print("Tapped none")
    }
  }

}

extension TraditionalRegistrationController: TextFieldDelegate {
  public func textFieldDidEndEditing(_ textField: UITextField) {
    (textField as? ErrorTextField)?.isErrorRevealed = false
  }
  
  public func textFieldShouldClear(_ textField: UITextField) -> Bool {
    (textField as? ErrorTextField)?.isErrorRevealed = false
    return true
  }
  
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    (textField as? ErrorTextField)?.isErrorRevealed = true
    return true
  }
}

extension UITapGestureRecognizer {
  
  func didTapAttributedTextInLabel(label: UILabel, inRange targetRange: NSRange) -> Bool {
    // Create instances of NSLayoutManager, NSTextContainer and NSTextStorage
    let layoutManager = NSLayoutManager()
    let textContainer = NSTextContainer(size: CGSize.zero)
    let textStorage = NSTextStorage(attributedString: label.attributedText!)
    
    // Configure layoutManager and textStorage
    layoutManager.addTextContainer(textContainer)
    textStorage.addLayoutManager(layoutManager)
    
    // Configure textContainer
    textContainer.lineFragmentPadding = 0.0
    textContainer.lineBreakMode = label.lineBreakMode
    textContainer.maximumNumberOfLines = label.numberOfLines
    let labelSize = label.bounds.size
    textContainer.size = labelSize
    
    // Find the tapped character location and compare it to the specified range
    let locationOfTouchInLabel = self.location(in: label)
    let textBoundingBox = layoutManager.usedRect(for: textContainer)
    let x = (labelSize.width - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x
    let y = (labelSize.height - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y
    let textContainerOffset = CGPoint(x: x, y: y)
    let locationOfTouchInTextContainer = CGPoint(x: locationOfTouchInLabel.x - textContainerOffset.x, y: locationOfTouchInLabel.y - textContainerOffset.y)
    let indexOfCharacter = layoutManager.characterIndex(for: locationOfTouchInTextContainer, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
    
    return NSLocationInRange(indexOfCharacter, targetRange)
  }
  
}
