//
//  HomeController.swift
//  AppetiserCodingChallenge
//
//  Created by Joenabie Gamao ( https://maxineer.com )
//  Copyright © 2018 "Appetiser Pty Ltd". All rights reserved.
//

import UIKit

class HomeController: UIViewController {
  override func viewDidLoad() {
    super.viewDidLoad()
//    navigationController?.pushViewController(R.storyboard.settings().NAC, animated: true)
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    navigationController?.setNavigationBarHidden(false, animated: true)
  }
  
}
