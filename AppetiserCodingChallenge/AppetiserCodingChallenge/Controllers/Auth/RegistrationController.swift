//
//  RegistrationController.swift
//  AppetiserCodingChallenge
//
//  Created by Joenabie Gamao ( https://maxineer.com )
//  Copyright © 2018 "Appetiser Pty Ltd". All rights reserved.
//

import UIKit

class RegistrationController: UIViewController {
  
  var email: String?
  
  @IBOutlet weak var emailTextField: FormTextField!
  @IBOutlet weak var passwordTextField: FormTextField!
  @IBOutlet weak var firstNameTextField: FormTextField!
  @IBOutlet weak var lastNameTextField: FormTextField!

  override func viewDidLoad() {
    super.viewDidLoad()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    navigationController?.setNavigationBarHidden(false, animated: animated)
    
    emailTextField.text = email
  }
  
}
