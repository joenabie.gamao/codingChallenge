//
//  PasswordResetController.swift
//  AppetiserCodingChallenge
//
//  Created by Joenabie Gamao ( https://maxineer.com )
//  Copyright © 2018 "Appetiser Pty Ltd". All rights reserved.
//

import UIKit

class PasswordResetController: UIViewController {

  var email: String?
  
  @IBOutlet weak var textField: FormTextField!
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    navigationController?.setNavigationBarHidden(false, animated: animated)
    
    textField.text = email
  }
  
  @IBAction
  func backButtonTapped(_ sender: AnyObject) {
    self.navigationController?.popViewController(animated: true)
  }
  
  @IBAction
  func sendButtonTapped(_ sender: AnyObject) {
    guard textField.text != nil else { return }
    
    // TODO Implement
    let alert = UIAlertController(
      title: "Password Reset Email Sent",
      message: "An email has been sent to your email address. Follow"
        + " the direction in the email to reset your password.",
      preferredStyle: .alert
    )
    alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (_) in
      self.navigationController?.popViewController(animated: true)
    }))
    present(alert, animated: true, completion: nil)
  }
  
}
