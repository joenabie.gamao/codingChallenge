//
//  LoginController.swift
//  AppetiserCodingChallenge
//
//  Created by Joenabie Gamao ( https://maxineer.com )
//  Copyright © 2018 "Appetiser Pty Ltd". All rights reserved.
//

import UIKit
import PureLayout

class LoginController: UIViewController {
  
  struct Segue {
    static let toRegistrationForm = "toRegistrationForm"
    static let toPasswordResetForm = "toPasswordResetForm"
  }
  
  enum Mode {
    case emailOnly
    case passwordOnly
  }
 
  @IBOutlet weak var textLabel: UILabel!
  @IBOutlet weak var textField: FormTextField!
  @IBOutlet weak var loginButton: FormButton!
  @IBOutlet weak var loginButtonBottomEdgeConstraint: NSLayoutConstraint!
  @IBOutlet weak var passwordResetButton: UIButton!
  
  var mode: Mode = .emailOnly
  var email: String?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    update()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    navigationController?.setNavigationBarHidden(true, animated: animated)
  }
  
  @IBAction
  func startButtonTapped(_ sender: AnyObject) {
    if mode == .emailOnly {
      // TODO Check whether email is already linked.
      if textField.text == "example@domain.com" {
        email = textField.text
        mode = .passwordOnly
        update()
      } else {
        email = textField.text
        performSegue(withIdentifier: Segue.toRegistrationForm, sender: sender)
      }
    } else {
      // TODO Check whether password matches the account's password.
      if textField.text == "test123" {
//        navigationController?.setViewControllers([
//          MainStoryboard.mainController(),
//        ], animated: true)
      }
    }
  }
  
  @IBAction
  func passwordResetButtonTapped(_ sender: AnyObject) {
    performSegue(withIdentifier: Segue.toPasswordResetForm, sender: sender)
  }
  
  func update() {
    if mode == .emailOnly {
      textLabel.text = "Enter your email to get started."
      
      textField.text = nil
      textField.placeholder = "Email"
      textField.isSecureTextEntry = false
      textField.accessibilityLabel = "Email"
      
      loginButton.setTitle("GET STARTED", for: .normal)
      loginButton.accessibilityLabel = "Next"
      loginButtonBottomEdgeConstraint.autoRemove()
      loginButtonBottomEdgeConstraint = loginButton.autoPinEdge(toSuperviewEdge: .bottom, withInset: 40)
      
      passwordResetButton.isHidden = true
      
    } else {
      textLabel.text = "Welcome back!"
      
      textField.text = nil
      textField.placeholder = "Password"
      textField.isSecureTextEntry = true
      textField.accessibilityLabel = "Password"
      
      loginButton.setTitle("LOGIN", for: .normal)
      loginButton.accessibilityLabel = "Login"
      loginButtonBottomEdgeConstraint.autoRemove()
      loginButtonBottomEdgeConstraint = loginButton
        .autoPinEdge(.bottom, to: .top, of: passwordResetButton, withOffset: -8)
      
      passwordResetButton.isHidden = false
    }
  }
  
  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    if let controller = segue.destination as? PasswordResetController {
      controller.email = email
    } else if let controller = segue.destination as? RegistrationController {
      controller.email = email
    }
  }
  
}
