//
//  APIClient+Account.swift
//  AppetiserCodingChallenge
//
//  Created by Joenabie Gamao ( https://maxineer.com )
//  Copyright © 2019 "Appetiser Pty Ltd". All rights reserved.
//

import Foundation
import Alamofire

// MARK: - Account
extension APIClient {
  
  /// Authenticates the specified login credentials of the guest user.
  @discardableResult
  func login(
    _ email: String,
    password: String,
    completion: @escaping APIClientLoginClosure
  ) -> DataRequest {
    return request(
      "auth/login",
      method: .post,
      parameters: ["email": email, "password": password],
      encoding: URLEncoding.httpBody,
      headers: httpRequestHeaders(withAuth: false),
      success: { resp in
        guard let dict = resp.data as? JSONDictionary else {
          return completion(nil, nil, APIClientError.dataNotFound(JSONDictionary.self))
        }
        completion(resp.decodedValue(forKeyPath: "user"), dict["token"] as? String, nil)
      }, failure: { error in
        completion(nil, nil, error)
      })
  }

  /// This checks whether the specified email is still available for a new account or
  /// is already paired to an existing account.
  ///
  /// When checking for completion block arguments, make sure to check first the `error` argument.
  ///
  @discardableResult
  func checkEmailAvailability(
    _ email: String,
    completion: @escaping (_ available: Bool, _ error: Error?) -> Void
  ) -> DataRequest {
    return request(
      "auth/email/check",
      method: .post,
      parameters: ["email": email],
      encoding: URLEncoding.httpBody,
      headers: httpRequestHeaders(withAuth: false),
      success: { resp in
        guard let dict = resp.data as? JSONDictionary else {
          return completion(true, APIClientError.dataNotFound(JSONDictionary.self))
        }
        let isExisting = (dict["email_exists"] as? Bool) ?? false
        completion(!isExisting, nil)
      }, failure: { error in
        completion(true, error)
      })
  }
  
  /// Creates new user and returns a user data and token.
  @discardableResult
  func register(
    _ email: String,
    password: String,
    completion: @escaping APIClientLoginClosure
  ) -> DataRequest {
    return request(
      "auth/register",
      method: .post,
      parameters: [
        "email": email,
        "password": password,
        "password_confirmation": password
      ],
      encoding: URLEncoding.httpBody,
      headers: httpRequestHeaders(withAuth: false),
      success: { resp in
        guard let dict = resp.data as? JSONDictionary else {
          return completion(nil, nil, APIClientError.dataNotFound(JSONDictionary.self))
        }
        completion(resp.decodedValue(forKeyPath: "user"), dict["token"] as? String, nil)
      }, failure: { error in
        completion(nil, nil, error)
      })
  }
  
  @discardableResult
  func connectFacebookAccount(
    _ accessToken: String,
    completion: @escaping APIClientLoginClosure
  ) -> DataRequest {
    return request(
      "auth/connect/facebook",
      method: .post,
      parameters: ["fb_token": accessToken],
      encoding: URLEncoding.httpBody,
      headers: httpRequestHeaders(withAuth: false),
      success: { resp in
        guard let dict = resp.data as? JSONDictionary else {
          return completion(nil, nil, APIClientError.dataNotFound(JSONDictionary.self))
        }
        completion(resp.decodedValue(forKeyPath: "user"), dict["token"] as? String, nil)
      }, failure: { error in
        completion(nil, nil, error)
      })
  }
  
  @discardableResult
  func logout(_ completion: @escaping FailureClosure) -> DataRequest {
    return request("auth/logout", method: .get, success: { resp in
      completion(nil)
    }, failure: { error in
      completion(error)
    })
  }
  
}

// MARK: - Password Reset
extension APIClient {
  
  /// Request for Password Reset token. To be sent to the specified email address.
  @discardableResult
  func requestForPasswordResetToken(
    _ email: String,
    completion: @escaping (_ success: Bool, Error?) -> Void
  ) -> DataRequest {
    return request(
      "password/email",
      method: .post,
      parameters: ["email": email],
      encoding: URLEncoding.httpBody,
      headers: httpRequestHeaders(withAuth: false),
      success: { resp in
        completion(resp.status == .ok, nil)
      }, failure: { error in
        completion(false, error)
      })
  }
  
  /// Update user's account password.
  ///
  /// - parameter email: The user's email address.
  /// - parameter token: The token sent to user's email when he requested for password reset.
  /// - parameter password: The new password.
  ///
  @discardableResult
  func updatePassword(
    _ email: String,
    token: String,
    password: String,
    completion: @escaping (_ success: Bool, Error?) -> Void
  ) -> DataRequest {
    return request(
      "password/reset",
      method: .post,
      parameters: [
        "email": email,
        "token": token,
        "password": password,
        "password_confirmation": password
      ],
      encoding: URLEncoding.httpBody,
      headers: httpRequestHeaders(withAuth: false),
      success: { resp in
        completion(resp.status == .ok, nil)
      }, failure: { error in
        completion(false, error)
      })
  }
  
}
