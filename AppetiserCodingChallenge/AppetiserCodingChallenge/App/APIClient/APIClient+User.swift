//
//  APIClient+User.swift
//  AppetiserCodingChallenge
//
//  Created by Joenabie Gamao ( https://maxineer.com )
//  Copyright © 2019 "Appetiser Pty Ltd". All rights reserved.
//

import Foundation
import Alamofire

// MARK: - User Info
extension APIClient {
  
  @discardableResult
  func updateUserInfo(
    _ params: UserInfoRequestParams,
    completion: @escaping APIClientUserClosure
  ) -> DataRequest {
    return request(
      "user",
      method: .put,
      parameters: params.dictionary(),
      success: { resp in
        completion(resp.decodedValue(forKeyPath: "user"), nil)
      }, failure: { error in
        completion(nil, error)
      })
  }
  
  @discardableResult
  func fetchUserInfo(_ completion: @escaping APIClientUserClosure) -> DataRequest {
    return request("auth/user", method: .get, success: { resp in
      completion(resp.decodedValue(forKeyPath: "user"), nil)
    }, failure: { error in
      completion(nil, error)
    })
  }
  
  @discardableResult
  func requestPhoneVerifCode(_ completion: @escaping FailureClosure) -> DataRequest {
    return request(
      "auth/mobile/verification/resend",
      method: .post,
      success: { _ in
        completion(nil)
      }, failure: { error in
        completion(error)
      })
  }
  
  @discardableResult
  func verifyPhoneNumber(_ code: String, completion: @escaping APIClientUserClosure) -> DataRequest {
    return request(
      "auth/mobile/verify",
      method: .post,
      parameters: ["verification_code": code],
      success: { resp in
        completion(resp.decodedValue(forKeyPath: "user"), nil)
      }, failure: { error in
        completion(nil, error)
      })
  }
  
}
