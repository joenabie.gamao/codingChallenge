//
//  APIClient+SearchItunes.swift
//  AppetiserCodingChallenge
//
//  Created by Joenabie Gamao on 12/11/2019.
//  Copyright © 2019 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import Alamofire

// MARK: Search iTunes
extension APIClient {
  
  /// Search iTunes API with default params, country = "au", media = "movie"
  /// - Parameter query: Search Term
  /// - Parameter completion: Return either an array of Album or an Error
  func searchItunes(query: String, completion: @escaping APISearchItunesClosure) {
    let searchString = query.replacingOccurrences(of: " ", with: "+")
    guard let url = URL(string: URL.base + URL.search) else {
      return completion(nil, nil)
    }
    let params = SearchInfoRequestParams(term: searchString, country: URL.defaultCountry, media: URL.defaultMedia)
    // Using Alamofire in this case instead of from API client because this is a 3rd party API that doesn't have same response with what we have in the boilerplate
    Alamofire.request(
              url,
              method: .get,
              parameters: params.dictionary(),
              headers: httpRequestHeaders(withAuth: false)).responseData { (response) in
        // Catch Error
        if let err = response.error {
          completion(nil, err)
        }
                
        // Data
        if let rdata = response.data, let albums = Album.decodeArray(data: rdata) {
            completion(albums, nil)
        }
                        
        // Nothing
        completion(nil, nil)
    }
  }
}
