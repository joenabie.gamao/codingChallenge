//
//  SearchInfoRequestParams.swift
//  AppetiserCodingChallenge
//
//  Created by Joenabie Gamao on 18/11/2019.
//  Copyright © 2019 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

struct SearchInfoRequestParams: APIRequestParameters, Codable {
  var term: String?
  var country: String?
  var media: String?
}
