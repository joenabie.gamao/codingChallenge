//
//  BaseModelTrackingService.swift
//  QuestionInfluencers
//
//  Created by Joenabie Gamao ( https://maxineer.com )
//  Copyright © 2019 "Appetiser Pty Ltd". All rights reserved.
//

import Foundation
import Alamofire

/// A type which can be bookmarked, followed, or faved by the currently logged in user.
protocol TrackableModel: Model, Identifiable {
  
  var isTracking: Bool { get }
  
}

enum ModelTrackingState {
  case tracking
  case tracked
  case untracking
  case untracked
}

class ModelTrackingInfo {
  var modelId: String
  var state: ModelTrackingState = .untracked
  var request: DataRequest?

  init<T>(_ model: T) where T: TrackableModel {
    self.modelId = model.id
    self.state = model.isTracking ? .tracked : .untracked
  }
}

protocol ModelTrackingService: AppService {
  
  /// Use this to add the model you'd like to track.
  func addModel<T>(_ model: T) where T: TrackableModel
  
  func addModels<T>(_ models: [T]) where T: TrackableModel
  
  /// Use this in cases where:
  /// - a model was permanently deleted.
  func removeModel(with id: String)
  
  /// Make sure to call this once the current user logs out.
  func removeAllModels()
  
  /// For states `tracking` and `untracking`, they'll be considered as `tracked` and `untracked`
  /// respectively. This is to simplify the handling in the call site.
  func isTrackingModel(with id: String) -> Bool
  
  /// Track or untrack a model for the currently logged in user.
  func toggleModelTrackingState(with id: String)
  
}

struct ModelTrackingServiceNotifications {
  
  /// A Notification sent after the API request has completed processing.
  ///
  /// UserInfo keys:
  /// - `model_id`: String. Id of the model being updated.
  /// - `error`: Error. Any error encountered when attempting to update the model.
  static let didUpdateModel = Notification.Name(rawValue: "ModelTrackingService.didUpdateModel")
  
}

/// Provides the app a sort of convenience layer for tracking or untracking `TrackableModel`s.
///
/// Advantages of having this layer instead of directly calling the endpoint wrappers:
/// - One place implementation for tracking/untracking a particular type of model.
/// - Changes to tracking status of the model is shared throughout the app via NSNotification.
/// - Changes can be displayed on the view instantaneously without having to wait for the request to complete.
/// - Takes care of the in-transit api request cancellation in case the user quickly changes his mind.
///
class BaseModelTrackingService: ModelTrackingService {
  
  private var modelMap: [String: ModelTrackingInfo] = [:]
  
  init() {
    NotificationCenter.default.addObserver(
      self,
      selector: #selector(appUserLoggedOut),
      name: AppUser.Notifications.didLogout,
      object: nil
    )
  }
  
  @objc private func appUserLoggedOut() {
    removeAllModels()
  }
  
  func addModel<T>(_ model: T) where T: TrackableModel {
    guard modelMap[model.id] == nil else { return }
    modelMap[model.id] = ModelTrackingInfo(model)
  }
  
  func addModels<T>(_ models: [T]) where T: TrackableModel {
    models.forEach { self.addModel($0) }
  }
  
  func removeModel(with id: String) {
    modelMap.removeValue(forKey: id)
  }
  
  func removeAllModels() {
    modelMap = [:]
  }
  
  func isTrackingModel(with id: String) -> Bool {
    guard let info = modelMap[id] else { return false }
    return info.state == .tracked || info.state == .tracking
  }
  
  func toggleModelTrackingState(with id: String) {
    guard let info = modelMap[id] else { return }
    if info.state == .tracked {
      untrackModel(info)
    } else if info.state == .untracked {
      trackModel(info)
    } else {
      rollbackModelState(info)
    }
  }
  
  func modelTrackingRequest(_ info: ModelTrackingInfo, completion: @escaping FailureClosure) -> DataRequest {
    fatalError("Subclass should implement this")
  }
  
  func modelUntrackingRequest(_ info: ModelTrackingInfo, completion: @escaping FailureClosure) -> DataRequest {
    fatalError("Subclass should implement this")
  }
  
}

// MARK: - Private Helpers
private extension BaseModelTrackingService {
  
  func trackModel(_ info: ModelTrackingInfo) {
    info.state = .tracking
    info.request = modelTrackingRequest(info, completion: { [weak self] (error) in
      guard let slf = self else { return }
      
      info.state = error == nil ? .tracked : .untracked
      info.request = nil
      
      NotificationCenter.default.post(
        name: ModelTrackingServiceNotifications.didUpdateModel,
        object: slf,
        userInfo: [
          "model_id": info.modelId,
          "error": error ?? false
        ]
      )
    })
  }
  
  func untrackModel(_ info: ModelTrackingInfo) {
    info.state = .untracking
    info.request = modelUntrackingRequest(info, completion: { [weak self] (error) in
      guard let slf = self else { return }
      
      info.state = error == nil ? .untracked : .tracked
      info.request = nil
      
      NotificationCenter.default.post(
        name: ModelTrackingServiceNotifications.didUpdateModel,
        object: slf,
        userInfo: [
          "model_id": info.modelId,
          "error": error ?? false
        ]
      )
    })
  }
  
  func rollbackModelState(_ info: ModelTrackingInfo) {
    if let req = info.request {
      req.cancel()
      info.request = nil
    }
    
    switch info.state {
    case .tracking, .untracked:
      info.state = .untracked
    case .untracking, .tracked:
      info.state = .tracked
    }

    NotificationCenter.default.post(
      name: ModelTrackingServiceNotifications.didUpdateModel,
      object: self,
      userInfo: [
        "model_id": info.modelId,
        "error": false
      ]
    )
  }
  
}

// MARK: - Example Subclass

/*
class UserBookmarkingService: BaseModelTrackingService {
 
  override func modelTrackingRequest(
    _ info: ModelTrackingInfo, completion: @escaping (Error?) -> Void
  ) -> DataRequest {
    return app.api.bookmarkUser(info.modelId, completion: completion)
  }
 
  override func modelUntrackingRequest(
    _ info: ModelTrackingInfo, completion: @escaping (Error?) -> Void
  ) -> DataRequest {
    return app.api.unbookmarkUser(info.modelId, completion: completion)
  }
 
}
*/
