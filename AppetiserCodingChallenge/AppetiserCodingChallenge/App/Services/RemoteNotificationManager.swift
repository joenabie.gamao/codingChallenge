//
//  RemoteNotificationManager.swift
//  AppetiserCodingChallenge
//
//  Created by Joenabie Gamao ( https://maxineer.com )
//  Copyright © 2019 "Appetiser Pty Ltd". All rights reserved.
//

import UIKit
import UserNotifications

class RemoteNotificationManager: AppService {
  
  fileprivate var userInfo: [AnyHashable: Any]?
  
  init() {
    setupNotificationObservers()
  }
  
  private func setupNotificationObservers() {
    debugLog("")
    
    let nc = NotificationCenter.default
    let opQueue = OperationQueue.main
    
    _ = nc.addObserver(
      forName: AppUser.Notifications.didLogin,
      object: App.shared.user,
      queue: opQueue,
      using: { [weak self] _ in
        self?.requestPermissionToShowPushNotifications()
    })
  }
  
}

// MARK: - General Setup
extension RemoteNotificationManager {
  
  /// Set of authorization options that the app needs approval from the user.
  var userNotificationAuthorizationOptions: UNAuthorizationOptions {
    return UNAuthorizationOptions(arrayLiteral: [.alert, .badge, .sound])
  }
  
  func getAuthorizationStatus(_ handler: @escaping (UNAuthorizationStatus) -> Void) {
    UNUserNotificationCenter.current().getNotificationSettings { (settings) in
      handler(settings.authorizationStatus)
    }
  }
  
  func requestPermissionToShowPushNotifications() {
    guard app.user.isLoggedIn else { return }
    debugLog("")
    
    UNUserNotificationCenter.current().requestAuthorization(
      options: userNotificationAuthorizationOptions
    ) { [weak self] granted, error in
      
      debugLog("Permission granted: \(granted)")
      guard granted else { return }
      self?.registerForRemoteNotificationsIfAuthorized()
    }
  }
  
  func registerForRemoteNotificationsIfAuthorized() {
    debugLog("")
    UNUserNotificationCenter.current().getNotificationSettings { settings in
      debugLog("Notification settings: \(settings)")
      guard settings.authorizationStatus == .authorized else { return }
      DispatchQueue.main.async {
        UIApplication.shared.registerForRemoteNotifications()
      }
    }
  }
  
}

// MARK: - Notification Handling
extension RemoteNotificationManager {
  
  /// Stores the notification info for subsequent processing.
  ///
  /// Example expected info:
  /// ```
  ///  {
  ///    "aps": {
  ///      "alert": "Hey there!",
  ///      "sound": "default"
  ///    }
  ///  }
  /// ```
  /// Google FCM notification:
  /// ```
  ///  {
  ///    "google.c.a.e": 1,
  ///    "aps": {
  ///      "alert": {
  ///        "body": "John Appleseed invited you to join his project as a recorder.",
  ///        "title": "CrowdFilm"
  ///      },
  ///      "badge": 1
  ///    },
  ///    "gcm.message_id": "0:1551945601979719%914b2324914b2324"
  ///  }
  /// ```
  func storeNotification(_ userInfo: [AnyHashable: Any]) {
    debugLog(String(describing: userInfo))
    self.userInfo = userInfo
  }
  
  @discardableResult
  func processStoredNotification() -> Bool {
    guard let info = self.userInfo else {
      return false
    }
    guard let aps = info["aps"] as? JSONDictionary else {
      return false
    }
    debugLog(String(describing: aps))
    
    // Make sure to clear this data so we won't accidentally re-process it.
    self.userInfo = nil
    
    let application = UIApplication.shared
    
    if application.applicationState == .active {
      // Application is in foreground when remote notification arrived.
    } else if application.applicationState == .background || application.applicationState == .inactive {
      //
    }
    
    return true
  }
  
}
