//
//  AppUser.swift
//  AppetiserCodingChallenge
//
//  Created by Joenabie Gamao ( https://maxineer.com )
//  Copyright © 2019 "Appetiser Pty Ltd". All rights reserved.
//

import Foundation

class AppUser: AppService {
  
  struct Notifications {}
  
  private let encodedUserInfoKey = "encoded_user_info"
  
  static let facebookReadPermissions = ["public_profile", "email"]
  
  private(set) var user: User?

  var isLoggedIn: Bool {
    return user != nil && App.shared.api.accessToken != nil
  }
  
  init() {
    if let userInfo = encodedUserInfo {
      do {
        debugLog("<-- \(try JSONSerialization.jsonObject(with: userInfo))")
        user = try User.decode(userInfo)
      } catch DecodingError.typeMismatch {
        // Force remove cached user info since we can't decode it anymore.
        encodedUserInfo = nil
      } catch {
        App.shared.recordError(error)
      }
    }
  }
  
}

// MARK: - Notifications
extension AppUser.Notifications {
  
  static let didLogin = Notification
    .Name(rawValue: "com.AppetiserCodingChallenge.notification.name.appUser.didLogin")
  static let didLogout = Notification
    .Name(rawValue: "com.AppetiserCodingChallenge.notification.name.appUser.didLogout")
  static let didRefreshUser = Notification
    .Name(rawValue: "com.AppetiserCodingChallenge.notification.name.appUser.didRefreshUser")
  
}

// MARK: - User Authentication Layers
extension AppUser {
  
  func login(with email: String, password: String, completion: @escaping APIClientUserClosure) {
    app.api.login(email, password: password, completion: loginHandler(completion))
  }
  
  func register(with email: String, password: String, completion: @escaping APIClientUserClosure) {
    app.api.register(email, password: password, completion: loginHandler(completion))
  }
  
  func connectFacebook(with token: String, completion: @escaping APIClientUserClosure) {
    app.api.connectFacebookAccount(token, completion: loginHandler(completion))
  }
  
  private func loginHandler(_ completion: @escaping APIClientUserClosure) -> APIClientLoginClosure {
    return { (user, token, error) in
      guard let token = token, error == nil else {
        return completion(nil, error)
      }
      self.user = user
      self.app.api.accessToken = token
      
      do {
        self.encodedUserInfo = try user?.encode()
        completion(user, nil)
        NotificationCenter.default.post(name: Notifications.didLogin, object: self)
        
      } catch {
        App.shared.recordError(error)
        completion(nil, error)
      }
    }
  }
  
  func logout(_ completion: @escaping FailureClosure) {
    App.shared.api.logout { (error) in
      guard error == nil else { return completion(error) }
      
      self.user = nil
      self.encodedUserInfo = nil
      self.app.api.accessToken = nil
      
      completion(nil)
      
      NotificationCenter.default.post(name: Notifications.didLogout, object: self)
    }
  }
  
}

// MARK: - User Info
extension AppUser {
  
  func updateUserInfo(with params: UserInfoRequestParams, completion: @escaping APIClientUserClosure) {
    app.api.updateUserInfo(params, completion: userInfoUpdateHandler(completion))
  }
  
  func refreshUserInfo(_ completion: @escaping APIClientUserClosure) {
    app.api.fetchUserInfo(userInfoUpdateHandler(completion))
  }
  
  func verifyPhoneVerificationCode(with code: String, completion: @escaping APIClientUserClosure) {
    app.api.verifyPhoneNumber(code, completion: userInfoUpdateHandler(completion))
  }
  
  private func userInfoUpdateHandler(_ completion: @escaping APIClientUserClosure) -> APIClientUserClosure {
    return { (user, error) in
      guard error == nil else {
        return completion(nil, error)
      }
      debugLog("-- user info updated: \n\(String(describing: user!.dictionary()))")
      self.user = user
      
      do {
        self.encodedUserInfo = try user?.encode()
        completion(user, nil)
        NotificationCenter.default.post(name: Notifications.didRefreshUser, object: self)
        
      } catch {
        App.shared.recordError(error)
        completion(nil, error)
      }
    }
  }
  
}

// MARK: - Private Attributes and Helpers
private extension AppUser {
  
  var encodedUserInfo: Data? {
    set { UserDefaults.standard.set(newValue, forKey: encodedUserInfoKey) }
    get { return UserDefaults.standard.data(forKey: encodedUserInfoKey) }
  }
  
}
