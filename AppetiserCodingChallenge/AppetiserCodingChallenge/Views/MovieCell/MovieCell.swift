//
//  Cell.swift
//  AppetiserCodingChallenge
//
//  Created by Joenabie Gamao on 14/11/2019.
//  Copyright © 2019 Appetiser Pty Ltd. All rights reserved.
//

import UIKit
import PureLayout

// Album's Master TableView Cell
class MovieCell: UITableViewCell {
  
  @IBOutlet var innerContentView: UIView!
  @IBOutlet weak var albumImageView: UIImageView!
  @IBOutlet weak var trackNameLabel: UILabel!
  @IBOutlet weak var genreLabel: UILabel!
  @IBOutlet weak var priceLabel: UILabel!
  
  var viewModel: AlbumViewModel! {
      didSet {
        setupVM()
      }
   }
  
  override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
    super.init(style: .subtitle, reuseIdentifier: reuseIdentifier)
    onInit()
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    onInit()
  }
  
  func onInit() {
    Bundle.main.loadNibNamed("MovieCell", owner: self, options: nil)
    contentView.addSubview(innerContentView)
    innerContentView.autoPinEdgesToSuperviewEdges()
    
    setup()
  }
  
  func setup() {
    albumImageView.contentMode = .scaleAspectFill
    albumImageView.image = #imageLiteral(resourceName: "empty.png")
    accessoryType = .none
    selectionStyle = .none
  }
  
  func setupVM() {
    if let url = viewModel.artwork100URL {
      albumImageView.af_setImage(withURL: url)
    }
    priceLabel.attributedText = viewModel.priceAttributed()
    trackNameLabel.attributedText = viewModel.trackNameAttributed()
    genreLabel.attributedText = viewModel.genreAttributed()
  }
  
}
