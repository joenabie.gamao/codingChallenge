//
//  NavigationController.swift
//  AppetiserCodingChallenge
//
//  Created by Joenabie Gamao ( https://maxineer.com )
//  Copyright © 2019 "Appetiser Pty Ltd". All rights reserved.
//

import UIKit

class NavigationController: UINavigationController {
  
  override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
    if let controller = visibleViewController {
      return controller.supportedInterfaceOrientations
    }
    return super.supportedInterfaceOrientations
  }
  
  override var shouldAutorotate: Bool {
    if let controller = visibleViewController {
      return controller.shouldAutorotate
    }
    return super.shouldAutorotate
  }
  
  override var preferredStatusBarStyle: UIStatusBarStyle {
    if let controller = visibleViewController {
      return controller.preferredStatusBarStyle
    }
    return super.preferredStatusBarStyle
  }
  
}
