//
//  DynamicValue.swift
//  AppetiserCodingChallenge
//
//  Created by Joenabie Gamao on 12/11/2019.
//  Copyright © 2019 Appetiser Pty Ltd. All rights reserved.
//

import Foundation


class DynamicValue<T> {
    typealias CompletionHandler = ((T) -> Void)
    
    var value: T {
        didSet {
            notify()
        }
    }
    
    private var observers = [String: CompletionHandler]()
    
    init(_ value: T) {
        self.value = value
    }
    
    public func addObserver(_ observer: NSObject, completionHandler: @escaping CompletionHandler) {
        observers[observer.description] = completionHandler
    }
    
    func addAndNotify(observer: NSObject, completionHandler: @escaping CompletionHandler) {
        addObserver(observer, completionHandler: completionHandler)
        notify()
    }
    
    func notify() {
        observers.forEach {
            $0.value(value)
        }
    }
    
    deinit {
        observers.removeAll()
    }
}
