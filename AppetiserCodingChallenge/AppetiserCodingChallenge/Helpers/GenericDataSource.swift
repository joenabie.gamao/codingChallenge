//
//  GenericDataSource.swift
//  AppetiserCodingChallenge
//
//  Created by Joenabie Gamao on 12/11/2019.
//  Copyright © 2019 Appetiser Pty Ltd. All rights reserved.
//

import Foundation

// To separate the ViewModel to the data layer. We can reuse regardless the data we would like to update.
class GenericDataSource<T>: NSObject {
    var data: DynamicValue<[T]> = DynamicValue([])
}
