//
//  Identifiable.swift
//  AppetiserCodingChallenge
//
//  Created by Joenabie Gamao ( https://maxineer.com )
//  Copyright © 2019 "Appetiser Pty Ltd". All rights reserved.
//

import Foundation

protocol Identifiable {
  
  var id: String { get }
  
}

extension Equatable where Self: Identifiable {
  
  static func == (lhs: Self, rhs: Self) -> Bool {
    return lhs.id == rhs.id
  }
  
}
