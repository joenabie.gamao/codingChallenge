//
//  UITableViewCell.swift
//  AppetiserCodingChallenge
//
//  Created by Joenabie Gamao on 18/11/2019.
//  Copyright © 2019 Appetiser Pty Ltd. All rights reserved.
//

import UIKit

extension UITableViewCell {
  static var reuseIdentifier: String {
    return String(describing: self)
  }
}
