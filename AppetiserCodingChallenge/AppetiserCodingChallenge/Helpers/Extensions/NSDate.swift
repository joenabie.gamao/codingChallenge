//
//  NSDate.swift
//  AppetiserCodingChallenge
//
//  Created by Joenabie Gamao ( https://maxineer.com )
//  Copyright © 2018 "Appetiser Pty Ltd". All rights reserved.
//

import Foundation

extension Date {
  
  func dblog() -> String {
    return Constants.Formatters.debugConsoleDateFormatter.string(from: self)
  }
  
  var displayString: String {
    return Constants.Formatters.debugConsoleDateFormatter.string(from: self)
  }
  
}
