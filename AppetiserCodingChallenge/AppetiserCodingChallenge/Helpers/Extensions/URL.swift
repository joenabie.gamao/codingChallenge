//
//  URL.swift
//  AppetiserCodingChallenge
//
//  Created by Joenabie Gamao ( https://maxineer.com )
//  Copyright © 2019 "Appetiser Pty Ltd". All rights reserved.
//

import Foundation

// MARK: - iTunes URLs
extension URL {
  static var base: String {
      return "https://itunes.apple.com"
  }

  static var defaultCountry: String {
      return "au"
  }

  static var defaultMedia: String {
      return "movie"
  }

  static var defaultTerm: String {
    return "star"
  }

  static var search: String {
    return "/search"
  }
}
