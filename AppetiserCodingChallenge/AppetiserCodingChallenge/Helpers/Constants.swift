//
//  Constants.swift
//  AppetiserCodingChallenge
//
//  Created by Joenabie Gamao ( https://maxineer.com )
//  Copyright © 2018 "Appetiser Pty Ltd". All rights reserved.
//

import Foundation

public typealias JSONDictionary = [String: Any]

typealias S = R.string.localizable // swiftlint:disable:this type_name

/// Typealias for closures that take Void argument and return Void type.
typealias VoidClosure = () -> Void
/// Typealias for closures that take an optional Error argument and return a Void type.
typealias FailureClosure = (Error?) -> Void

struct Constants {
  
  struct UserDefaults {
      static let lastSavedAlbum = "lastSavedAlbum"
      static let lastSavedTrackName = "lastVisitedTrackName"
      static let lastSavedPrice = "lastVisitedPrice"
      static let lastSavedGenre = "lastVisitedGenre"
      static let lastSavedImageURL = "lastVisitedImageURL"
      static let lastSavedDate = "lastVisitedDate"
      static let lastSavedLongDesc = "lastSavedLongDesc"
  }
  
  struct Formatters {
    
    static let debugConsoleDateFormatter: DateFormatter = {
      let formatter = DateFormatter()
      formatter.dateFormat = "yyyy-MM-dd HH:mm:ss.SSS"
      formatter.timeZone = TimeZone(identifier: "UTC")!
      return formatter
    }()
    
    /// 02/06/1980 10:00 PM
    static let defaultDateFormatter: DateFormatter = {
      let formatter = DateFormatter()
      formatter.dateFormat = "MM/dd/yy h:mm a"
      formatter.timeZone = TimeZone(identifier: "UTC")!
      return formatter
    }()
    
  }
  
}
