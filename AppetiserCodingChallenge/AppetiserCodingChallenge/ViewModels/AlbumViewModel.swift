//
//  AlbumViewModel.swift
//  AppetiserCodingChallenge
//
//  Created by Joenabie Gamao on 12/11/2019.
//  Copyright © 2019 Appetiser Pty Ltd. All rights reserved.
//

import UIKit

class AlbumViewModel: NSObject {
  let trackName: String
  let artwork100: String
  let price: Double
  let genre: String
  let longDescription: String
  let album: Album
  let lastVisitedDate: Date
  let fromUserDefaults: Bool
  
  var priceStr: String {
    return "$\(price)"
  }
    
  init(album: Album, lastVisitedDate: Date = Date(), fromUserDefaults: Bool = false) {
      self.album = album
      self.genre = album.primaryGenreName
      self.artwork100 = album.artworkUrl100
      self.trackName = album.trackName
      self.price = album.trackPrice
      self.longDescription = album.longDescription
      self.lastVisitedDate = lastVisitedDate
      self.fromUserDefaults = fromUserDefaults
  }
  
  func lastVisitedDateAttributed(alignment: NSTextAlignment = .right) -> NSAttributedString? {
      if fromUserDefaults {
          return "Last Visited: \(lastVisitedDate.displayString)".attributed(
            font: UIFont.systemFont(ofSize: 14),
            color: .gray, alignment: alignment)
      }
      return nil
  }

  var artwork100URL: URL? {
    return URL(string: artwork100)
  }

  var hqImageURL: URL? {
      let replaced = artwork100.replacingOccurrences(of: "100x100bb.jpg", with: "300x300bb.jpg")
      return URL(string: replaced)
  }
  
  func saveToUserDefaults() {
    let userDefaults = UserDefaults.standard
    if let data = try? JSONEncoder().encode(album) {
      userDefaults.set(data, forKey: Constants.UserDefaults.lastSavedAlbum)
      userDefaults.set(Date(), forKey: Constants.UserDefaults.lastSavedDate)
    }
//      userDefaults.set(trackName, forKey: Constants.UserDefaults.lastSavedTrackName)
//      userDefaults.set(artwork100, forKey: Constants.UserDefaults.lastSavedImageURL)
//      userDefaults.set(genre, forKey: Constants.UserDefaults.lastSavedGenre)
//      userDefaults.set(price, forKey: Constants.UserDefaults.lastSavedPrice)
//      userDefaults.set(longDescription, forKey: Constants.UserDefaults.lastSavedLongDesc)
  }
}

// MARK: Attributed Texts
extension AlbumViewModel {
  func trackNameAttributed(alignment: NSTextAlignment = .left) -> NSAttributedString {
    return trackName.attributed(font: UIFont.boldSystemFont(ofSize: 15), alignment: alignment)
  }
  
  func genreAttributed(alignment: NSTextAlignment = .left) -> NSAttributedString {
    return genre.attributed(font: UIFont.systemFont(ofSize: 14), alignment: alignment)
  }
  
  func priceAttributed(alignment: NSTextAlignment = .left) -> NSAttributedString {
    return priceStr.attributed(font: UIFont.systemFont(ofSize: 14), alignment: alignment)
  }
}
