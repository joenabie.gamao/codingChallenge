source 'https://github.com/CocoaPods/Specs'

platform :ios, '11.0'
use_frameworks!

# Add Application pods here
def app_pods
  
  pod 'Alamofire', '4.8.2'
  pod 'AlamofireImage', '~> 3.5.2'
  pod 'AlamofireNetworkActivityIndicator', '~> 2.4.0'
  pod 'ChameleonFramework/Swift', :git => 'https://github.com/ViccAlexander/Chameleon.git'
  pod 'IQKeyboardManagerSwift', '~> 6.2.1'
  pod 'Material', '~> 3.1.6'
  pod 'PureLayout', '~> 3.1.4'
  pod 'R.swift', '~> 5.0.3'
  pod 'SVProgressHUD', '~> 2.2.5'
  pod 'SwiftDate', '~> 5.1.0'
  pod 'SwiftLint', '~> 0.34.0'
  pod 'Valet', '~> 3.2.5'
  
  # ReactiveX
  pod 'RxSwift', '5.0.0'
  pod 'RxSwiftExt', '5.0.0'
  pod 'NSObject+Rx', '5.0.0'
  
  # Google
  pod 'GooglePlaces', '~> 3.2.0'
  pod 'Fabric', '~> 1.10.2'
  pod 'Crashlytics', '~> 3.13.4'
  
  # Firebase
  #pod 'Firebase/Core', '~> 6.5.0'
  #pod 'Firebase/Auth', '~> 6.5.0'
  #pod 'Firebase/Firestore', '~> 6.5.0'
  #pod 'CodableFirebase', '~> 0.2.0'
  
end

def testing_pods
  pod 'Quick', '~> 2.1.0'
  pod 'Nimble', '~> 8.0.2'
  pod 'Mockingjay', '~> 2.0.1'
end

target 'AppetiserCodingChallenge' do
  app_pods
end

target 'AppetiserCodingChallenge-Staging' do
  app_pods
end

target 'Tests' do
  app_pods
  testing_pods
end

target 'UITests' do
  testing_pods
end

post_install do |installer|
  
  puts "  Removing static analyzer support for all targets."
  installer.pods_project.targets.each do |target|
    target.build_configurations.each do |config|
        config.build_settings['OTHER_CFLAGS'] = "$(inherited) -Qunused-arguments -Xanalyzer -analyzer-disable-all-checks"
    end
  end
  
#  puts "  Using Swift 5.0 for targets:"
#  installer.pods_project.targets.each do |target|
#    if ['NameOfPodHere'].include? target.name
#      puts "   - #{target.name}"
#      target.build_configurations.each do |config|
#        config.build_settings['SWIFT_VERSION'] = '5.0'
#      end
#    end
#  end

end
